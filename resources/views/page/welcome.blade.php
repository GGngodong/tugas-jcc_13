@extends('layout.master')
@section('judul_1')
Selamat Datang {{$name_f}} {{$name_l}} ...
@endsection
@section('judul_2')
Welcome Back!    
@endsection

@section('content')
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="links">
            <a>Terimakasih telah bergabung di Sanberbook. Social Media Kita bersama!</a>
        </div>
    </div>
</div>
@endsection
