<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');

Route::get('/register', 'AuthController@index');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/cast/create', 'CastController@create');
Route::post('/cast','CastController@store');
Route::get('/cast','CastController@index');
Route::get('/cast/{id_cast}','CastController@show');
Route::get('/cast/{id_cast}/edit','CastController@edit');
Route::put('/cast/{id_cast}','CastController@update');
Route::delete('/cast/{id_cast}','CastController@destroy');

Route::get('/table', function(){
    return view('page.table');
});

Route::get('/data-tables', function(){
    return view('page.data-table');
});
